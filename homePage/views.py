from django.shortcuts import render

# Create your views here.
def homePage(request):
    return render(request, 'homePage.html')

def mainPage(request):
    return render(request, 'mainPage.html')

def formPage(request):
    return render(request, 'formPage.html')
    