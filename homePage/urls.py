from django.urls import path
from . import views

app_name = 'homePage'

urlpatterns = [
    path('', views.homePage, name='homePage'),
    path('mainPage/', views.mainPage, name='mainPage'),
    path('formPage/', views.formPage, name='formPage'),
]