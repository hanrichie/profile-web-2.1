from django.db import models

# Create your models here.
class Schedule(models.Model):
    day = models.CharField(max_length=30)
    date = models.DateField(max_length=30, null=True)
    time = models.TimeField(max_length=30, null=True)
    name = models.CharField(max_length=30)
    place = models.CharField(max_length=30)
    categories = models.CharField(max_length=30)