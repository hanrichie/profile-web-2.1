from django.shortcuts import render, redirect
from .forms import AddSchedule
from .models import Schedule

app_name = 'homepage'

# Create your views here.
response = {}
def schedule(request):
    response['jadwal'] = Schedule.objects.all().values()
    return render(request, 'schedule.html', response)

def addSchedule(request):
    form = AddSchedule(request.POST)
    if(request.method == 'POST' and form.is_valid()):
        model = Schedule(
            day = request.POST['day'],
            date = request.POST['date'],
            time = request.POST['time'],
            name = request.POST['name'],
            place = request.POST['place'],
            categories = request.POST['categories']
        )
        model.save()
    return redirect("addSchedule")

def clearSchedule(request):
    response = {}
    response['jadwal'] = Schedule.objects.all().delete()
    return redirect('schedule')