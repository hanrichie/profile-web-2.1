from django.urls import path
from .views import schedule, addSchedule, clearSchedule

urlpatterns = [
    path('', schedule, name='schedule'),
    path('add/', addSchedule, name='addSchedule'),
    path('clear/', clearSchedule, name='clearSchedule'),
]